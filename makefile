BACKEND_DIR := backend/
FRONTEND_DIR := frontend/

ifeq ($(shell uname), Darwin)          # Apple
    PYTHON   := python3
    PIP      := pip3
    PYLINT   := pylint
    COVERAGE := coverage
    PYDOC    := pydoc3
    AUTOPEP8 := autopep8
else ifeq ($(shell uname -p), unknown) # Windows
    PYTHON   := python                 # on my machine it's python
    PIP      := pip3
    PYLINT   := pylint
    COVERAGE := coverage
    PYDOC    := python -m pydoc        # on my machine it's pydoc
    AUTOPEP8 := autopep8
else                                   # UTCS
    PYTHON   := python3
    PIP      := pip3
    PYLINT   := pylint3
    COVERAGE := coverage
    PYDOC    := pydoc3
    AUTOPEP8 := autopep8
endif

clean:
	rm -f .coverage
	rm -rf __pycache__
	rm -f *.pyc

IDB1.log:
	git log --all > IDB1.log

unittest:
	cd ./backend/ &&  python3 test.py

test: IDB1.log tests

run:
	$(MAKE) -C $(BACKEND_DIR) -s run & $(MAKE) -C $(FRONTEND_DIR) -s run