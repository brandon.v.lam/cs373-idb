# -------
# imports
# -------

from unittest import main, TestCase
import requests
from testHelper import sortBy

STATUS_OK = 200


class TestMain (TestCase):
    # ---------
    # Location
    # ---------

    def test_Location_status(self):
        response = requests.get(
            "https://idb-3-354621.uc.r.appspot.com/locations")
        self.assertEqual(response.status_code, STATUS_OK)

    def test_Location_content_size(self):
        response = requests.get(
            "https://idb-3-354621.uc.r.appspot.com/locations")
        self.assertEqual(len(response.json()["Locations"]), 29)

    def test_Location_check(self):
        response = requests.get(
            "http://idb-3-354621.uc.r.appspot.com/locations")
        self.assertEqual(
            response.json()["Locations"][0]["city"], "Cupertino")

    def test_Location_check1(self):
        response = requests.get(
            "http://idb-3-354621.uc.r.appspot.com/locations")
        self.assertEqual(
            response.json()["Locations"][1]["city"], "Seattle")

    def test_Location_check2(self):
        response = requests.get(
            "http://idb-3-354621.uc.r.appspot.com/locations")
        self.assertEqual(
            response.json()["Locations"][2]["city"], "Dallas")

    def test_Location_check3(self):
        response = requests.get(
            "http://idb-3-354621.uc.r.appspot.com/locations")
        self.assertEqual(
            response.json()["Locations"][3]["city"], "Chicago")

    def test_Location_filter(self):
        response = requests.get(
            "http://idb-3-354621.uc.r.appspot.com/locations")
        self.assertEqual(
            sortBy(response.json()["Locations"], "state")[0]["state"], "Arizona")

    def test_Location_search(self):
        response = requests.get(
            "https://idb-3-354621.uc.r.appspot.com/locations/?search=california")
        self.assertEqual(
            response.json()["Locations"][0]["state"], "California")

    def test_Location_search1(self):
        response = requests.get(
            "https://idb-3-354621.uc.r.appspot.com/locations/?search=los%20angeles")
        self.assertEqual(
            response.json()["Locations"][0]["city"], "Los Angeles")

    def test_Location_search2(self):
        response = requests.get(
            "https://idb-3-354621.uc.r.appspot.com/locations/?search=los%20angeles")
        self.assertEqual(response.json()["Locations"][0]["country"], "US")

    def test_Location_search_size(self):
        response = requests.get(
            "https://idb-3-354621.uc.r.appspot.com/locations/?search=california")
        self.assertEqual(len(response.json()["Locations"]), 6)

    def test_Location_search_size1(self):
        response = requests.get(
            "https://idb-3-354621.uc.r.appspot.com/locations/?search=us")
        self.assertEqual(len(response.json()["Locations"]), 29)

    def test_Location_search_size2(self):
        response = requests.get(
            "https://idb-3-354621.uc.r.appspot.com/locations/?search=Seattle")
        self.assertEqual(len(response.json()["Locations"]), 1)

    def test_Location_search_size3(self):
        response = requests.get(
            "https://idb-3-354621.uc.r.appspot.com/locations/?search=Austin")
        self.assertEqual(len(response.json()["Locations"]), 1)

    # ---------
    # Company
    # ---------

    def test_Company_status(self):
        response = requests.get(
            "https://idb-3-354621.uc.r.appspot.com/companies")
        self.assertEqual(response.status_code, STATUS_OK)

    def test_Company_content_size(self):
        response = requests.get(
            "https://idb-3-354621.uc.r.appspot.com/companies")
        self.assertEqual(len(response.json()["Companies"]), 23)

    def test_Company_check(self):
        response = requests.get(
            "https://idb-3-354621.uc.r.appspot.com/companies")
        self.assertEqual(response.json()["Companies"][0]["name"], "Apple")

    def test_Company_check1(self):
        response = requests.get(
            "https://idb-3-354621.uc.r.appspot.com/companies")
        self.assertEqual(response.json()["Companies"][1]["name"], "Amazon")

    def test_Company_check2(self):
        response = requests.get(
            "https://idb-3-354621.uc.r.appspot.com/companies")
        self.assertEqual(
            response.json()["Companies"][2]["name"], "Goldman Sachs")

    def test_Company_check3(self):
        response = requests.get(
            "https://idb-3-354621.uc.r.appspot.com/companies")
        self.assertEqual(response.json()["Companies"][3]["name"], "Boeing")

    def test_Location_filter(self):
        response = requests.get(
            "http://idb-3-354621.uc.r.appspot.com/companies")
        self.assertEqual(
            sortBy(response.json()["Companies"], "name")[0]["name"], "Amazon")

    def test_Company_search(self):
        response = requests.get(
            "https://idb-3-354621.uc.r.appspot.com/companies/?search=amazon")
        self.assertEqual(response.json()["Companies"][0]["name"], "Amazon")

    def test_Company_search1(self):
        response = requests.get(
            "https://idb-3-354621.uc.r.appspot.com/companies/?search=technology")
        self.assertEqual(response.json()["Companies"][3]["name"], "Dropbox")

    def test_Company_search2(self):
        response = requests.get(
            "https://idb-3-354621.uc.r.appspot.com/companies/?search=technology")
        self.assertEqual(response.json()["Companies"][4]["name"], "USAA")

    def test_Company_search_size(self):
        response = requests.get(
            "https://idb-3-354621.uc.r.appspot.com/companies/?search=technology")
        self.assertEqual(len(response.json()["Companies"]), 13)

    def test_Company_search_size1(self):
        response = requests.get(
            "https://idb-3-354621.uc.r.appspot.com/companies/?search=pinterest")
        self.assertEqual(len(response.json()["Companies"]), 1)

    def test_Company_search_size2(self):
        response = requests.get(
            "https://idb-3-354621.uc.r.appspot.com/companies/?search=Metlife")
        self.assertEqual(len(response.json()["Companies"]), 1)

    def test_Company_search_size3(self):
        response = requests.get(
            "https://idb-3-354621.uc.r.appspot.com/companies/?search=Intel")
        self.assertEqual(len(response.json()["Companies"]), 1)

    def test_Company_search_size4(self):
        response = requests.get(
            "https://idb-3-354621.uc.r.appspot.com/companies/?search=fnjdnfakjs")
        self.assertEqual(len(response.json()["Companies"]), 0)

    def test_Company_search_size5(self):
        response = requests.get(
            "https://idb-3-354621.uc.r.appspot.com/companies/?search=Fin")
        self.assertEqual(len(response.json()["Companies"]), 6)

    def test_Company_search_size6(self):
        response = requests.get(
            "https://idb-3-354621.uc.r.appspot.com/companies/?search=text")
        self.assertEqual(len(response.json()["Companies"]), 1)

    # ---------
    # Job
    # ---------
    def test_Job_status(self):
        response = requests.get("http://idb-3-354621.uc.r.appspot.com/jobs")
        self.assertEqual(response.status_code, STATUS_OK)

    def test_Job_content_size(self):
        response = requests.get("https://idb-3-354621.uc.r.appspot.com/jobs")
        self.assertEqual(len(response.json()["Jobs"]), 24)

    def test_Job_check(self):
        response = requests.get("https://idb-3-354621.uc.r.appspot.com/jobs")
        self.assertEqual(response.json()[
                         "Jobs"][0]["title"], "Amazon Picker")

    def test_Job_check1(self):
        response = requests.get("https://idb-3-354621.uc.r.appspot.com/jobs")
        self.assertEqual(response.json()[
                         "Jobs"][1]["title"], "CWM- Marcus by Goldman Sachs- Savings Specialist- Richardson, TX")

    def test_Job_check2(self):
        response = requests.get("https://idb-3-354621.uc.r.appspot.com/jobs")
        self.assertEqual(response.json()[
                         "Jobs"][2]["title"], "Boeing Research & Technology Manufacturing Technology Integration - Commercial Manager")

    def test_Job_check3(self):
        response = requests.get("https://idb-3-354621.uc.r.appspot.com/jobs")
        self.assertEqual(response.json()[
                         "Jobs"][3]["title"], "Boeing Research & Technology Manufacturing Technology Integration - Commercial Manager")

    def test_Location_filter(self):
        response = requests.get(
            "http://idb-3-354621.uc.r.appspot.com/jobs")
        self.assertEqual(
            sortBy(response.json()["Jobs"], "category")[0]["category"], "Accounting & Finance Jobs")

    def test_Job_search(self):
        response = requests.get(
            "https://idb-3-354621.uc.r.appspot.com/jobs/?search=amazon")
        self.assertEqual(response.json()["Jobs"][0]["title"], "Amazon Picker")

    def test_Job_search1(self):
        response = requests.get(
            "https://idb-3-354621.uc.r.appspot.com/jobs/?search=engineer")
        self.assertEqual(response.json()[
                         "Jobs"][1]["title"], "Boeing Research & Technology Manufacturing Technology Integration - Commercial Manager")

    def test_Job_search2(self):
        response = requests.get(
            "https://idb-3-354621.uc.r.appspot.com/jobs/?search=engineer")
        self.assertEqual(response.json()[
                         "Jobs"][2]["title"], "Boeing Research & Technology Manufacturing Technology Integration - Commercial Manager")

    def test_Job_search3(self):
        response = requests.get(
            "https://idb-3-354621.uc.r.appspot.com/jobs/?search=engineer")
        self.assertEqual(response.json()[
                         "Jobs"][3]["title"], "Corporate Media and Collaborations Systems Manager")

    def test_Job_search_size(self):
        response = requests.get(
            "https://idb-3-354621.uc.r.appspot.com/jobs/?search=uber")
        self.assertEqual(len(response.json()["Jobs"]), 3)

    def test_Job_search_size1(self):
        response = requests.get(
            "https://idb-3-354621.uc.r.appspot.com/jobs/?search=pinterest")
        self.assertEqual(len(response.json()["Jobs"]), 5)

    def test_Job_search_size2(self):
        response = requests.get(
            "https://idb-3-354621.uc.r.appspot.com/jobs/?search=CWM")
        self.assertEqual(len(response.json()["Jobs"]), 1)

    def test_Job_search_size3(self):
        response = requests.get(
            "https://idb-3-354621.uc.r.appspot.com/jobs/?search=Sales")
        self.assertEqual(len(response.json()["Jobs"]), 3)

    def test_Job_search_size4(self):
        response = requests.get(
            "https://idb-3-354621.uc.r.appspot.com/jobs/?search=Dell")
        self.assertEqual(len(response.json()["Jobs"]), 1)

    def test_Job_search_size5(self):
        response = requests.get(
            "https://idb-3-354621.uc.r.appspot.com/jobs/?search=Research")
        self.assertEqual(len(response.json()["Jobs"]), 3)

    def test_Job_search_size6(self):
        response = requests.get(
            "https://idb-3-354621.uc.r.appspot.com/jobs/?search=APP")
        self.assertEqual(len(response.json()["Jobs"]), 3)

    def test_Job_search_size7(self):
        response = requests.get(
            "https://idb-3-354621.uc.r.appspot.com/jobs/?search=Job")
        self.assertEqual(len(response.json()["Jobs"]), 24)

# ----
# main
# ----


if __name__ == "__main__":
    main()
