from flask import Flask, jsonify
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import relationship
from flask_cors import CORS


import os

# initializing Flask app
app = Flask(__name__)
CORS(app)
# Change this accordingly
USER = "postgres"
PASSWORD = "Password"
PUBLIC_IP_ADDRESS = "34.72.151.166"
DBNAME = "findjobsdb"


# Configuration
app.config['SQLALCHEMY_DATABASE_URI'] = \
    os.environ.get(
        "DB_STRING", f'postgresql://{USER}:{PASSWORD}@{PUBLIC_IP_ADDRESS}/{DBNAME}')

# To suppress a warning message
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)


class Location(db.Model):
    """
    Location of jobs
    Coordinates used for Google Map
    Can contain many jobs and many companies
    """
    __tablename__ = "location"
    id = db.Column(db.Integer(), primary_key=True)
    city = db.Column(db.String())
    state = db.Column(db.String())
    country = db.Column(db.String())
    latitude = db.Column(db.String())
    longitude = db.Column(db.String())
    temperature = db.Column(db.String())
    weather = db.Column(db.String())
    photo = db.Column(db.String())
    jobs = relationship("Job", back_populates="location")
    companies = relationship("Company", back_populates="location")

    def serialize(self):
        """
        formats response as: 
        id
        city
        state
        country
        latitude
        longitude
        temperature
        weather
        photo
        jobs
        companies
        """
        def format_company_response(company):
            return{
                'id': company.id,
                'name': company.name
            }

        def format_job_response(job):
            return{
                'id': job.id,
                'title': job.title
            }

        return {
            'id': self.id,
            'city': self.city,
            'state': self.state,
            'country': self.country,
            'latitude': self.latitude,
            'longitude': self.longitude,
            'temperature': self.temperature,
            'weather': self.weather,
            'photo': self.photo,
            'jobs': list([format_job_response(job) for job in self.jobs]),
            'companies': list([format_company_response(company) for company in self.companies]),
        }


class Company(db.Model):
    """
    Companies of jobs
    Can contain many jobs and one location
    """
    __tablename__ = "company"
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String())
    description = db.Column(db.String())
    industry = db.Column(db.String())
    logo = db.Column(db.String())
    jobs = relationship("Job", back_populates="company")
    location_id = db.Column(db.Integer(), db.ForeignKey("location.id"))
    location = relationship("Location", back_populates="companies")

    def serialize(self):
        """
        formats response as
        id
        name
        description
        industry
        logo
        location
        jobs
        """
        def format_job_response(job):
            return{
                'id': job.id,
                'title': job.title
            }

        return {
            'id': self.id,
            'name': self.name,
            'description': self.description,
            'industry': self.industry,
            'logo': self.logo,
            'location': {"city": self.location.city, "state": self.location.state, "id": self.location_id},
            'jobs': list([format_job_response(job) for job in self.jobs]),
        }


class Job(db.Model):
    """
    Jobs
    Can contain one company and one location
    """
    __tablename__ = "job"
    id = db.Column(db.Integer(), primary_key=True)
    title = db.Column(db.String())
    description = db.Column(db.String())
    category = db.Column(db.String())
    job_url = db.Column(db.String())
    created_at = db.Column(db.String())
    company_id = db.Column(db.Integer(), db.ForeignKey("company.id"))
    company_name = db.Column(db.String())
    company = relationship("Company", back_populates="jobs")
    location_id = db.Column(db.Integer(), db.ForeignKey("location.id"))
    location = relationship("Location", back_populates="jobs")

    def serialize(self):
        """
        formats response as
        id
        title
        description
        category
        jobUrl
        createdAt
        companyName
        company
        location
        """
        return {
            'id': self.id,
            'title': self.title,
            'description': self.description,
            'category': self.category,
            'jobUrl': self.job_url,
            'createdAt': self.created_at,
            'companyName': self.company_name,
            'company': {"name": self.company.name, "id": self.company_id},
            'location': {"city": self.location.city, "state": self.location.state, "id": self.location_id}
        }
