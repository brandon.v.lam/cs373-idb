def sortBy(list, attr, reverse = False):
	return sorted(list, reverse = reverse, key = lambda x: x[attr])
