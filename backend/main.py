# main.py (v2.0)
from flask import jsonify
from create_db import create_companies, create_jobs, create_locations
from models import app, db, Company, Job, Location
from subprocess import Popen, PIPE
from sqlalchemy import or_
from flask import request


about_list = [{'name': 'Brandon Lam', 'img': 'brandonl.jpg', 'about_me': 'I am a junior at the University of Texas at Austin majoring in Computer Science. Outside of school, I play violin and video games for fun!', 'responsibilities': 'Developer, Team Lead', 'commits': '63', 'issues': '16', 'unit_tests': '4'},
              {'name': 'Blake Senn', 'img': 'blake.png', 'about_me': 'I am a senior at the University of Texas at Austin pursuing a degree in Computer Science and a minor in Economics. I am interested in full-stack development and machine learning. Outside of school I enjoy hiking, golf, and video games.',
                  'responsibilities': 'Developer - FE & BE, Researcher', 'commits': '77', 'issues': '16', 'unit_tests': '17'},
              {'name': 'Eileen Wang', 'img': 'eileen.png', 'about_me': 'I\'m a third year Computer Science student at The University of Texas at Austin, pursuing a minor in Economics and a certificate in Applied Statistical Modeling. Outside of school, I enjoy reading, skating, and playing video games.',
               'responsibilities': 'Developer', 'commits': '22', 'issues': '8', 'unit_tests': '9'},
              {'name': 'Jincheng Cao', 'img': 'Jincheng.png', 'about_me': 'I am a senior at University of Texas at Austin in Mathematics. I am interested in theory and applications of machine learning. Outside of school I enjoy playing basketball, skiing, and cooking.',
               'responsibilities': 'Developer', 'commits': '12', 'issues': '5', 'unit_tests': '9'},
              {'name': 'Eder Martinez', 'img': 'eder.png', 'about_me': 'I am a third year Computer Science student. I want to pursue a minor in the Arabic language. When I have time, I enjoy playing video games.',
               'responsibilities': 'Developer', 'commits': '10', 'issues': '4', 'unit_tests': '10'},
              {'name': 'Euie Son', 'img': 'euie.png', 'about_me': 'I\'m Euie, a 3rd year Computer Science student at the University of Texas at Austin, pursuing minors in Critical Disability Studies and Korean. I love art and playing music.', 'responsibilities': 'Developer', 'commits': '16', 'issues': '4', 'unit_tests': '2'}]


@app.route('/')
def index():
    response = jsonify("hello")
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route('/about/')
def about():
    response = jsonify(about_list)
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route('/companies/')
def companies():
    data = db.session.query(Company)
    query = request.args.get("search")
    data = search_companies(data, query)

    response = jsonify(Companies=[e.serialize() for e in data])
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


def search_companies(data, query):
    if query == None:
        return data.all()
    search_terms = query.split(" ")
    search = []
    for term in search_terms:
        search.append(Company.name.ilike("%{}%".format(term)))
        search.append(Company.description.ilike("%{}%".format(term)))
        search.append(Company.industry.ilike("%{}%".format(term)))
    data = data.filter(or_(*search))
    return data


@app.route('/companies/<int:id>')
def company(id):
    data = db.session.query(Company).filter(Company.id == id).one()
    response = jsonify(Company=data.serialize())
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route('/jobs/')
def jobs():
    data = db.session.query(Job)
    query = request.args.get("search")
    data = search_jobs(data, query)

    response = jsonify(Jobs=[e.serialize() for e in data])
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


def search_jobs(data, query):
    if query == None:
        return data.all()
    search_terms = query.split(" ")
    search = []
    for term in search_terms:
        search.append(Job.title.ilike("%{}%".format(term)))
        search.append(Job.description.ilike("%{}%".format(term)))
        search.append(Job.category.ilike("%{}%".format(term)))
        search.append(Job.company_name.ilike("%{}%".format(term)))
    data = data.filter(or_(*search))
    return data


@app.route('/jobs/<int:id>')
def job(id):
    data = db.session.query(Job).filter(Job.id == id).one()
    response = jsonify(Job=data.serialize())
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route('/models/')
def model():
    jobs_data = db.session.query(Job).all()
    jobs_response = [e.serialize() for e in jobs_data]

    companies_data = db.session.query(Company).all()
    companies_response = [e.serialize() for e in companies_data]

    locations_data = db.session.query(Location).all()
    locations_response = [e.serialize() for e in locations_data]

    response = jsonify(
        Jobs=jobs_response, Companies=companies_response, Locations=locations_response)
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route('/locations/')
def locations():
    data = db.session.query(Location)
    query = request.args.get("search")
    data = search_locations(data, query)

    response = jsonify(Locations=[e.serialize() for e in data])
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


def search_locations(data, query):
    if query == None:
        return data.all()
    search_terms = query.split(" ")
    search = []
    for term in search_terms:
        search.append(Location.city.ilike("%{}%".format(term)))
        search.append(Location.state.ilike("%{}%".format(term)))
        search.append(Location.country.ilike("%{}%".format(term)))
        search.append(Location.latitude.ilike("%{}%".format(term)))
        search.append(Location.longitude.ilike("%{}%".format(term)))
        search.append(Location.temperature.ilike("%{}%".format(term)))
        search.append(Location.weather.ilike("%{}%".format(term)))
    data = data.filter(or_(*search))
    return data


@app.route('/locations/<int:id>')
def location(id):
    data = db.session.query(Location).filter(Location.id == id).one()
    response = jsonify(Location=data.serialize())
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route('/search')
def search():
    query = request.args.get("search")

    companyData = db.session.query(Company)
    jobData = db.session.query(Job)
    locationData = db.session.query(Location)

    companies_data = search_companies(companyData, query)
    jobs_data = search_jobs(jobData, query)
    locations_data = search_locations(locationData, query)

    response = jsonify(Jobs=[e.serialize() for e in jobs_data], Companies=[e.serialize(
    ) for e in companies_data], Locations=[e.serialize() for e in locations_data])
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


# @app.route('/tests/')
# def tests():
#     p = Popen(["coverage", "run", "--branch", "test.py"], stderr=PIPE)
#     result = p.communicate()[1].decode()
#     response = jsonify(Results=result)
#     response.headers.add('Access-Control-Allow-Origin', '*')
#     return response
if __name__ == "__main__":
    db.drop_all()
    db.create_all()
    create_locations()
    create_companies()
    create_jobs()
    app.debug = True
    app.run()
