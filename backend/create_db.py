import json
from models import db, Job, Company, Location

# ------------
# load_json
# ------------


def load_json(filename):
    """
    return a python dict jsn
    filename a json file
    """
    with open(filename) as file:
        jsn = json.load(file)
        file.close()

    return jsn

# ------------
# create_books
# ------------


def create_jobs():
    """
    populate jobs table
    """
    jobs = load_json('./data/job.json')

    for job in jobs['results']:
        id = job['id']
        title = job['title']
        location_id = job['location_id']
        company_id = job['company_id']
        description = job['description']
        created_at = job['createdAt']
        company_name = job['company_name']
        category = job['category']
        job_url = job['jobUrl']

        newJob = Job(id=id, title=title, location_id=location_id,
                     company_id=company_id, description=description, created_at=created_at, company_name=company_name, category=category, job_url=job_url)

        # After I create the Job, I can then add it to my session.
        db.session.add(newJob)
        # commit the session to my DB.
        db.session.commit()


def create_companies():
    """
    populate companies table
    """
    companies = load_json('./data/company.json')

    for company in companies['results']:
        id = company['id']
        name = company['name']
        description = company['description']
        location_id = company['location_id']
        industry = company['industry']
        logo = company['logo']

        newCompany = Company(id=id, name=name, location_id=location_id,
                             description=description, industry=industry, logo=logo)

        # After I create the Company, I can then add it to my session.
        db.session.add(newCompany)
        # commit the session to my DB.
        db.session.commit()


def create_locations():
    """
    populate locations table
    """
    locations = load_json('./data/location.json')

    for location in locations['results']:
        id = location['id']
        city = location['city']
        state = location['state']
        country = location['country']
        latitude = location['latitude']
        longitude = location['longitude']
        temperature = location['temperature']
        weather = location['weather']
        photo = location['photo']

        newLocation = Location(id=id, city=city, state=state, country=country, latitude=latitude, longitude=longitude,
                               temperature=temperature, weather=weather, photo=photo)

        # After I create the Location, I can then add it to my session.
        db.session.add(newLocation)
        # commit the session to my DB.
        db.session.commit()
