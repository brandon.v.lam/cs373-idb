import axios from "axios";
import { useParams } from "react-router-dom";
import { useQuery } from "react-query";
import { useEffect } from "react";

async function getLocation(locationId) {
  try {
    const data = await axios.get(
      `https://idb-3-354621.uc.r.appspot.com/locations/${locationId}`
    );
    return data.data?.Location;
  } catch (err) {
    console.error(err);
  }
}

const LocationPage = () => {
  useEffect(() => {
    document.title = "Location";
  }, []);

  const params = useParams();

  const { data: location, isLoading } = useQuery(
    ["location", params.locationId],
    () => getLocation(params.locationId),
    {
      enabled: Boolean(params?.locationId),
    }
  );

  return (
    <div className="mt-4 px-4">
      {isLoading ? (
        <div aria-hidden="true">
          <div className="justify-content-center text-center fw-bold fs-2">
            <span className="placeholder col-2"></span>
            <span className="placeholder col-2"></span>
          </div>

          <div className="d-flex justify-content-center">
            <img
              className="img-fluid placeholder"
              style={{ width: "65%", height: "400px" }}
              vspace="10px"
              alt="loading"
            />
          </div>
          <div className="fs-5 fw-bold pt-4">State</div>
          <div className="fs-6 placeholder col-3"></div>
          <div className="fs-5 fw-bold pt-4">Country</div>
          <div className="fs-6 placeholder col-2"></div>
          <div className="fs-5 fw-bold pt-4">Coordinates</div>
          <div className="fs-6 placeholder col-4"></div>
          <div className="fs-5 fw-bold pt-4">Temperature and Weather</div>
          <div className="fs-6 placeholder col-4"></div>
          <div className="fs-5 fw-bold pt-4">Available Jobs</div>

          <div className="list-group">
            <span className="placeholder col-12"></span>
            <span className="placeholder col-12"></span>
            <span className="placeholder col-12"></span>
          </div>

          <div className="fs-5 fw-bold pt-4">Companies</div>

          <div className="list-group">
            <span className="placeholder col-12"></span>
            <span className="placeholder col-12"></span>
            <span className="placeholder col-12"></span>
          </div>
        </div>
      ) : (
        <>
          <div className="text-center fw-bold fs-2">{location.city}</div>
          <div className="d-flex justify-content-center">
            <img
              className="img-fluid"
              src={location.photo}
              alt={location.city}
              style={{ maxHeight: "400px" }}
              vspace="10px"
            />
          </div>
          <div className="fs-5 fw-bold pt-4">State</div>
          <div className="fs-6">{location.state}</div>
          <div className="fs-5 fw-bold pt-4">Country</div>
          <div className="fs-6">{location.country}</div>
          <div className="fs-5 fw-bold pt-4">Latitude</div>
          <div className="fs-6">{location.latitude}</div>
          <div className="fs-5 fw-bold pt-4">Longitude</div>
          <div className="fs-6">{location.longitude}</div>
          <div className="fs-5 fw-bold pt-4">Temperature</div>
          <div className="fs-6">{location.temperature}</div>
          <div className="fs-5 fw-bold pt-4">Weather</div>
          <div className="fs-6">{location.weather}</div>
          <div className="fs-5 fw-bold pt-4">Available Jobs</div>
          {location.jobs === [] ? (
            <p>No jobs available</p>
          ) : (
            <div className="list-group">
              {location.jobs.map((job) => (
                <a
                  href={`/jobs/${job.id}`}
                  className="list-group-item list-group-item-action"
                  key={job.id}
                >
                  {job.title}
                </a>
              ))}
            </div>
          )}

          <div className="fs-5 fw-bold pt-4">Companies</div>
          {location.companies === [] ? (
            <p>No companies available</p>
          ) : (
            <div className="list-group">
              {location.companies.map((company) => (
                <a
                  href={`/companies/${company.id}`}
                  key={company.id}
                  className="list-group-item list-group-item-action"
                >
                  {company.name}
                </a>
              ))}
            </div>
          )}

          <div className="fs-5 fw-bold pt-4">Map</div>
          <div className="d-flex justify-content-center">
            <iframe
              title={location.city}
              style={{}}
              aria-hidden="true"
              width="1000"
              height="400"
              loading="lazy"
              allowFullScreen
              referrerPolicy="no-referrer-when-downgrade"
              src={`https://www.google.com/maps/embed/v1/view?key=AIzaSyC_F3cnsypMvnJ7feEp60YwHKzhxEZwtRs&center=${location.latitude},${location.longitude}&zoom=12`}
            ></iframe>
          </div>

          <br></br>
        </>
      )}
    </div>
  );
};

export default LocationPage;
