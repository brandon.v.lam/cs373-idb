/* eslint-disable jsx-a11y/anchor-has-content */
import axios from "axios";
import { useMemo, useState } from "react";
import { useQuery } from "react-query";
import Table from "../../shared/Table";
import { useEffect } from "react";
import Highlighter from "../../shared/Highlighter";

async function getLocations(query = null) {
  try {
    const url = !query
      ? "https://idb-3-354621.uc.r.appspot.com/locations/"
      : "https://idb-3-354621.uc.r.appspot.com/locations?search=" + query;
    const data = await axios.get(url);
    return data.data;
  } catch (err) {
    console.error(err);
  }
}

const LocationsPage = () => {
  useEffect(() => {
    document.title = "Locations";
  }, []);

  const [query, setQuery] = useState(null);

  const { data, isLoading } = useQuery(["locations", query], () =>
    getLocations(query)
  );

  const columns = useMemo(
    () => [
      {
        Header: "City",
        accessor: "city",
      },
      {
        Header: "State",
        accessor: "state",
      },
      {
        Header: "Country",
        accessor: "country",
      },
      {
        Header: "Latitude",
        accessor: "latitude",
      },
      {
        Header: "Longitude",
        accessor: "longitude",
      },
      {
        Header: "Temperature (F)",
        accessor: "temperature",
      },
      {
        Header: "Weather",
        accessor: "weather",
      },
      {
        Header: "Jobs",
        accessor: "jobs",
        Cell: ({ value, row }) => {
          return value.map((job) => (
            <div key={job.id}>
              - <Highlighter wordsToHighlight={query}>{job.title}</Highlighter>
            </div>
          ));
        },
      },
    ],
    [query]
  );

  return (
    <div className="mt-4 px-4">
      <div className="text-center fw-bold fs-2">Locations</div>
      <Table
        data={data?.Locations || []}
        columns={columns}
        isLoading={isLoading}
        urlToNavigateTo={"/locations"}
        setQuery={setQuery}
        query={query}
      />
    </div>
  );
};

export default LocationsPage;
