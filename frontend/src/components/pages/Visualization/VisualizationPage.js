import { useEffect } from "react";
import AuthorVisualization from "./AuthorVisualization";
import BookVisualization from "./BookVisualization";
import GenreVisualization from "./GenreVisualization";

const VisualizationPage = () => {
  useEffect(() => {
    document.title = "Visualization";
  }, []);

  return (
    <div>
      <AuthorVisualization />
      <br></br>
      <BookVisualization />
      <br></br>
      <GenreVisualization />
      <br></br>
      <br></br>
    </div>
  );
};

export default VisualizationPage;
