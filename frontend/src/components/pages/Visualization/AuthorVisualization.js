import axios from "axios";
import ReactWordcloud from "react-wordcloud";
import { useQuery } from "react-query";
import { Spinner, Row } from "react-bootstrap";

const options = {
  colors: [
    "#f8971f",
    "#f8971f",
    "#ffd600",
    "#a6cd57",
    "#579d42",
    "#00a9b7",
    "#005f86",
  ],
  fontFamily: "sans-serif",
  fontSizes: [8, 24],
  rotationAngles: [0, 90],
  scale: "sqrt",
  spiral: "rectangular",
  transitionDuration: 3000,
  enableOptimizations: true,
};

async function getAuthors() {
  try {
    const data = await axios.get(
      "https://mediumdb-356314.uc.r.appspot.com/api/authors"
    );
    return data.data;
  } catch (err) {
    console.error(err);
  }
}

function AuthorVisualization() {
  const { data, isLoading } = useQuery("authors", getAuthors, {
    select: (data) => {
      return data.map((author) => ({
        text: author.author_name,
        value: 1,
      }));
    },
  });

  if (isLoading) {
    return (
      <div className="mt-4 px-4">
        <div className="text-center fw-bold fs-2">Authors</div>
        <Row className="justify-content-center">
          <Spinner animation="grow" />
        </Row>
      </div>
    );
  }
  return (
    <div className="mt-4 px-4" style={{ justifyContent: "center" }}>
      <div className="text-center fw-bold fs-2">Authors</div>
      <ReactWordcloud options={options} words={data || []} />
    </div>
  );
}

export default AuthorVisualization;
