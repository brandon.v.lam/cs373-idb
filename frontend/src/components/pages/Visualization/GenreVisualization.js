import axios from "axios";
import ReactWordcloud from "react-wordcloud";
import { useQuery } from "react-query";
import { Spinner, Row } from "react-bootstrap";

const options = {
  colors: [
    "#f8971f",
    "#f8971f",
    "#ffd600",
    "#a6cd57",
    "#579d42",
    "#00a9b7",
    "#005f86",
  ],
  fontFamily: "sans-serif",
  fontSizes: [8, 24],
  rotationAngles: [0, 90],
  scale: "sqrt",
  spiral: "rectangular",
  transitionDuration: 3000,
  enableOptimizations: true,
};

async function getGenres() {
  try {
    const data = await axios.get(
      "https://mediumdb-356314.uc.r.appspot.com/api/genres"
    );
    return data.data;
  } catch (err) {
    console.error(err);
  }
}

function GenreVisualization() {
  const transformBookData = (data) => {
    const temp = new Map();
    data.forEach((genre) => {
      temp.set(
        genre.genre_name.toLowerCase(),
        (temp.get(genre.genre_name.toLowerCase()) || 0) + 1
      );
    });
    return Array.from(temp, ([genre, value]) => ({
      text: genre,
      value: value,
    }));
  };

  const { data, isLoading } = useQuery("genres", getGenres, {
    select: (data) => transformBookData(data),
  });

  if (isLoading) {
    return (
      <div className="mt-4 px-4">
        <div className="text-center fw-bold fs-2">Genres</div>
        <Row className="justify-content-center">
          <Spinner animation="grow" />
        </Row>
      </div>
    );
  }

  return (
    <div className="mt-4 px-4">
      <div className="text-center fw-bold fs-2">Genres</div>
      <ReactWordcloud options={options} words={data || []} />
    </div>
  );
}

export default GenreVisualization;
