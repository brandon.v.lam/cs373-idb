import axios from "axios";
import ReactWordcloud from "react-wordcloud";
import { Spinner, Row } from "react-bootstrap";

import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
import { Bar } from "react-chartjs-2";
import { useQuery } from "react-query";

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);

const options = {
  colors: [
    "#f8971f",
    "#f8971f",
    "#ffd600",
    "#a6cd57",
    "#579d42",
    "#00a9b7",
    "#005f86",
  ],
  fontFamily: "sans-serif",
  fontSizes: [16, 32],
  rotationAngles: [0, 90],
  scale: "sqrt",
  spiral: "rectangular",
  transitionDuration: 3000,
  enableOptimizations: true,
};

async function getBooks() {
  try {
    const data = await axios.get(
      "https://mediumdb-356314.uc.r.appspot.com/api/books"
    );
    return data.data;
  } catch (err) {
    console.error(err);
  }
}

function BookVisualization() {
  const transformBookData = (data) => {
    const temp = new Map();

    const pageData = [0, 0, 0, 0, 0];

    data.forEach((book) => {
      const titleArray = book.book_title
        .toLowerCase()
        .replace(/[^\w\s]/gi, "")
        .split(" ");

      titleArray.forEach((word) => {
        const n = temp.get(word) || 0;
        temp.set(word, n + 1);
      });

      const pageNum = parseInt(book.book_page_count);
      if (pageNum && pageNum >= 0 && pageNum <= 999) {
        pageData[Math.floor(pageNum / 250)]++;
      } else {
        pageData[4]++;
      }
    });
    return {
      titles: Array.from(temp, ([word, value]) => ({
        text: word,
        value: value,
      })),
      pages: pageData,
    };
  };

  const { data, isLoading } = useQuery("books", getBooks, {
    select: (data) => transformBookData(data),
  });

  const pages = {
    labels: [
      "0 - 249 page",
      "250 - 499 page",
      "500 - 749 page",
      "750 - 999 page",
      "1000+ page",
      "n/a",
    ],
    datasets: [
      {
        label: "number of books",
        backgroundColor: "rgba(0,169,183,1)",
        borderColor: "rgba(0,0,0,1)",
        borderWidth: 3,
        data: data?.pages || [],
      },
    ],
  };

  if (isLoading) {
    return (
      <div className="mt-4 px-4">
        <div className="text-center fw-bold fs-2">Book Titles Word Cloud</div>
        <Row className="justify-content-center">
          <Spinner animation="grow" />
        </Row>
        <br></br>
        <div className="text-center fw-bold fs-2">Book Lengths</div>
        <Row className="justify-content-center">
          <Spinner animation="grow" />
        </Row>
      </div>
    );
  }

  return (
    <div className="mt-4 px-4">
      <div className="text-center fw-bold fs-2">Book Titles Word Cloud</div>
      <ReactWordcloud options={options} words={data?.titles || []} />
      <br></br>
      <div className="text-center fw-bold fs-2">Book Lengths</div>
      <Bar data={pages} />
    </div>
  );
}

export default BookVisualization;
