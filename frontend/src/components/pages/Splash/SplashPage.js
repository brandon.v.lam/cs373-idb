import { useEffect } from "react";

const SplashPage = () => {
  useEffect(() => {
    document.title = "Home";
  }, []);

  return (
    <>
      <div className="p-5 bg-dark text-white">
        <h1>Welcome to FindJobs</h1>
        <h5>We've got the right job for you!</h5>
      </div>
      <br />

      <div className="container-fluid" align="center">
        <div className="row g-3">
          <div className="col-sm">
            <div className="card sm">
              <div className="card-body">
                <h5 className="card-title">Jobs</h5>
                <p className="card-text">view job listings</p>
                <a href="/jobs" className="btn btn-primary">
                  view jobs
                </a>
              </div>
            </div>
          </div>
          <div className="col-sm">
            <div className="card sm">
              <div className="card-body">
                <h5 className="card-title">Companies</h5>
                <p className="card-text">find information about companies</p>
                <a href="/companies" className="btn btn-primary">
                  view companies
                </a>
              </div>
            </div>
          </div>
          <div className="col-sm">
            <div className="card sm">
              <div className="card-body">
                <h5 className="card-title">Locations</h5>
                <p className="card-text">discover locations of jobs</p>
                <a href="/locations" className="btn btn-primary">
                  view locations
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <br />

      <div
        id="carouselExampleControls"
        className="carousel slide"
        data-bs-ride="carousel"
      >
        <div className="carousel-inner" style={{ maxHeight: "600px" }}>
          <div className="carousel-item active">
            <img
              src={require("./about1.jpg")}
              className="d-block w-100"
              alt="img"
            />
          </div>
          <div className="carousel-item">
            <img
              src={require("./about2.jpg")}
              className="d-block w-100"
              alt="..."
            />
          </div>
          <div className="carousel-item">
            <img
              src={require("./image3.jpg")}
              className="d-block w-100"
              alt="..."
            />
          </div>
        </div>

        <button
          className="carousel-control-prev"
          type="button"
          data-bs-target="#carouselExampleControls"
          data-bs-slide="prev"
        >
          <span
            className="carousel-control-prev-icon"
            aria-hidden="true"
          ></span>
          <span className="visually-hidden">Previous</span>
        </button>
        <button
          className="carousel-control-next"
          type="button"
          data-bs-target="#carouselExampleControls"
          data-bs-slide="next"
        >
          <span
            className="carousel-control-next-icon"
            aria-hidden="true"
          ></span>
          <span className="visually-hidden">Next</span>
        </button>
      </div>
    </>
  );
};

export default SplashPage;
