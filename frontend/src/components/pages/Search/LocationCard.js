/* eslint-disable jsx-a11y/anchor-has-content */
import React from "react";
import Highlighter from "../../shared/Highlighter";

const LocationCard = ({ city, state, country, id, query }) => {
  return (
    <div
      className="card col m-3 custom-card"
      style={{ minWidth: "350px", maxWidth: "400px" }}
    >
      <div className="card-body">
        <h5 className="card-title">
          <Highlighter wordsToHighlight={query}>{city}</Highlighter>,{" "}
          <Highlighter wordsToHighlight={query}>{state}</Highlighter>
        </h5>
        <h6 className="card-subtitle mb-2 text-muted">
          <Highlighter wordsToHighlight={query}>{country}</Highlighter>
        </h6>
        <a href={`/locations/${id}`} className="stretched-link"></a>
      </div>
    </div>
  );
};

export default LocationCard;
