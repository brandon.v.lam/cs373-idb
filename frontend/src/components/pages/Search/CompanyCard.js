/* eslint-disable jsx-a11y/anchor-has-content */
import React from "react";
import Highlighter from "../../shared/Highlighter";

const CompanyCard = ({ name, description, industry, id, query }) => {
  return (
    <div
      className="card col m-3 custom-card"
      style={{ minWidth: "350px", maxWidth: "400px" }}
    >
      <div className="card-body">
        <h5 className="card-title">
          <Highlighter wordsToHighlight={query}>{name}</Highlighter>
        </h5>
        <p className="card-subtitle mb-2 text-muted">
          <Highlighter wordsToHighlight={query}>{industry}</Highlighter>
        </p>
        <p className="card-text">
          <Highlighter wordsToHighlight={query}>{description}</Highlighter>
        </p>
        <a href={`/companies/${id}`} className="stretched-link"></a>
      </div>
    </div>
  );
};

export default CompanyCard;
