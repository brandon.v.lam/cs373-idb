import React, { useState } from "react";
import axios from "axios";
import Search from "../../shared/Search";
import { useQuery } from "react-query";
import CompanyCard from "./CompanyCard";
import JobCard from "./JobCard";
import LocationCard from "./LocationCard";

async function globalSearch(query) {
  try {
    const url = "https://idb-3-354621.uc.r.appspot.com/search?search=" + query;
    const data = await axios.get(url);
    return data.data;
  } catch (err) {
    console.error(err);
  }
}
const SearchPage = () => {
  const [query, setQuery] = useState(null);

  const { data, isLoading } = useQuery(
    ["search", query],
    () => globalSearch(query),
    {
      enabled: Boolean(query),
    }
  );

  return (
    <div>
      <div className="d-flex justify-content-center pt-4">
        <h2>Search for Jobs / Companies / Locations</h2>
      </div>
      <div className="d-flex w-100 justify-content-center">
        <Search setQuery={setQuery} isLoading={isLoading} />
      </div>
      {data &&
      data?.Jobs?.length === 0 &&
      data?.Companies?.length === 0 &&
      data?.Locations?.length === 0 ? (
        <div className="d-flex w-100 justify-content-center pt-5">
          <h2>No results found</h2>
        </div>
      ) : data ? (
        <div className="px-5">
          {data?.Jobs.length > 0 && (
            <>
              <div className="d-flex justify-content-center pt-4">
                <h3>Jobs</h3>
              </div>
              <div className="row gx-5 justify-content-center">
                {data?.Jobs.map((searchResult, index) => (
                  <JobCard
                    key={index}
                    title={searchResult.title}
                    description={searchResult.description}
                    category={searchResult.category}
                    id={searchResult.id}
                    query={query}
                  />
                ))}
              </div>
            </>
          )}

          {data?.Companies.length > 0 && (
            <>
              <div className="d-flex justify-content-center pt-4">
                <h3>Companies</h3>
              </div>
              <div className="row gx-5 justify-content-center">
                {data?.Companies.map((searchResult, index) => (
                  <CompanyCard
                    key={index}
                    name={searchResult.name}
                    description={searchResult.description}
                    industry={searchResult.industry}
                    id={searchResult.id}
                    query={query}
                  />
                ))}
              </div>
            </>
          )}

          {data?.Locations.length > 0 && (
            <>
              <div className="d-flex justify-content-center pt-4">
                <h3>Locations</h3>
              </div>
              <div className="row gx-5 justify-content-center">
                {data?.Locations.map((searchResult, index) => (
                  <LocationCard
                    key={index}
                    city={searchResult.city}
                    state={searchResult.state}
                    country={searchResult.country}
                    id={searchResult.id}
                    query={query}
                  />
                ))}
              </div>
            </>
          )}
        </div>
      ) : null}
    </div>
  );
};

export default SearchPage;
