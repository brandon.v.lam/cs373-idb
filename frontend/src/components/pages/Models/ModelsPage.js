import axios from "axios";
import { useMemo, useEffect } from "react";
import { useQuery } from "react-query";
import Table from "../../shared/Table";

async function getModelsData() {
  try {
    const data = await axios.get(
      "https://idb-3-354621.uc.r.appspot.com/models/"
    );
    return data.data;
  } catch (err) {
    console.error(err);
  }
}

const ModelsPage = () => {
  useEffect(() => {
    document.title = "Models";
  }, []);

  // get data from our api
  const { data, isLoading } = useQuery("models", getModelsData);

  const jobsColumns = useMemo(
    () => [
      {
        Header: "#",
        accessor: "id",
      },
      {
        Header: "Title",
        accessor: "title",
      },
      {
        Header: "Description",
        accessor: "description",
      },
      {
        Header: "City",
        accessor: "location.city",
      },
      {
        Header: "State",
        accessor: "location.state",
      },
      {
        Header: "Company",
        accessor: "company.name",
      },
      {
        Header: "Category",
        accessor: "category",
      },
      {
        Header: "Date created",
        accessor: "createdAt",
      },
    ],
    []
  );

  const companiesColumns = useMemo(
    () => [
      {
        Header: "#",
        accessor: "id",
      },
      {
        Header: "Name",
        accessor: "name",
      },
      {
        Header: "Description",
        accessor: "description",
      },
      {
        Header: "City",
        accessor: "location.city",
      },
      {
        Header: "State",
        accessor: "location.state",
      },
      {
        Header: "Industry",
        accessor: "industry",
      },
      {
        Header: "Jobs",
        accessor: "jobs",
        Cell: ({ value, row }) => {
          return value.map((job) => <div key={job.id}>- {job.title}</div>);
        },
      },
    ],
    []
  );

  const locationsColumns = useMemo(
    () => [
      {
        Header: "#",
        accessor: "id",
      },
      {
        Header: "City",
        accessor: "city",
      },
      {
        Header: "State",
        accessor: "state",
      },
      {
        Header: "Country",
        accessor: "country",
      },
      {
        Header: "Latitude",
        accessor: "latitude",
      },
      {
        Header: "Longitude",
        accessor: "longitude",
      },
      {
        Header: "Temperature (F)",
        accessor: "temperature",
      },
      {
        Header: "Weather",
        accessor: "weather",
      },
      {
        Header: "Jobs",
        accessor: "jobs",
        Cell: ({ value, row }) => {
          return value.map((job) => <div key={job.id}>- {job.title}</div>);
        },
      },
    ],
    []
  );
  return (
    <div className="py-4">
      <div className="px-5">
        <h4>Jobs Model</h4>
        <div>
          <Table
            data={data?.Jobs || []}
            defaultPageSize={5}
            columns={jobsColumns}
            isLoading={isLoading}
            urlToNavigateTo={"/jobs"}
            showSearch={false}
          />
        </div>
      </div>

      <div className="px-5">
        <h4>Company Model</h4>
        <div>
          <Table
            data={data?.Companies || []}
            columns={companiesColumns}
            isLoading={isLoading}
            urlToNavigateTo={"/companies"}
            defaultPageSize={5}
            showSearch={false}
          />
        </div>
      </div>

      <div className="px-5">
        <h4>Location Model</h4>
        <div>
          <Table
            data={data?.Locations || []}
            columns={locationsColumns}
            isLoading={isLoading}
            urlToNavigateTo={"/locations"}
            defaultPageSize={5}
            showSearch={false}
          />
        </div>
      </div>
    </div>
  );
};

export default ModelsPage;
