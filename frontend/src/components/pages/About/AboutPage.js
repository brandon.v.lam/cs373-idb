import axios from "axios";
import { useState, useEffect } from "react";
import { useQuery } from "react-query";
import "./About.css";

async function getAbout() {
  try {
    const data = await axios.get(
      "https://idb-3-354621.uc.r.appspot.com/about/"
    );
    return data.data;
  } catch (err) {
    console.error(err);
  }
}

// async function runTests() {
//   try {
//     const data = await axios.get(
//       "https://idb-3-354621.uc.r.appspot.com/tests/"
//     );
//     return data.data;
//   } catch (err) {
//     console.error(err);
//   }
// }

const AboutPage = () => {
  useEffect(() => {
    document.title = "About";
  }, []);

  const { data: about, isLoading } = useQuery("about", getAbout);

  const [showTest, setShowTest] = useState(false);
  // const [isRunningTests, setIsRunningTests] = useState(false);
  // const [testResponse, setTestResponse] = useState(null);

  // const executeTests = async () => {
  //   setIsRunningTests(true);
  //   const data = await runTests();
  //   setTestResponse(data);
  //   setIsRunningTests(false);
  // };

  return (
    <>
      <div className="px-5 pt-5 pb-2">
        <button
          type="button"
          className="btn btn-primary"
          onClick={() => setShowTest((prev) => !prev)}
        >
          {showTest ? "Hide Test Image" : "Show Test Image"}
        </button>
      </div>
      {showTest && (
        <div className="px-5 pt-5 pb-4">
          <img src={require(`./tests.png`)} className="img-fluid" alt="tests" />
        </div>
      )}
      <div className="container align-center">
        <h5>Group Name: IDB-3</h5> <br />
      </div>
      <br />
      <div className="container overflow-hidden">
        {isLoading ? (
          <div>Loading About Info</div>
        ) : about === [] ? (
          <p>No information available</p>
        ) : about ? (
          <div className="wrapper">
            {about.map((member) => (
              <div className="col-sm d-flex" key={member.name}>
                <div className="card">
                  <img
                    className="card-img-top profile-img"
                    src={require(`./${member.img}`)}
                    alt={member.name}
                  />
                  <div className="p-3">
                    <h6 className="card-title">{member.name}</h6>
                    <p>{member.about_me}</p>
                    <p>Responsibilities: {member.responsibilities}</p>
                  </div>
                  <ul className="list-group list-group-flush">
                    <li className="list-group-item">
                      {member.commits} commits
                    </li>
                    <li className="list-group-item">{member.issues} issues</li>
                    <li className="list-group-item">
                      {member.unit_tests} unit tests
                    </li>
                  </ul>
                </div>
              </div>
            ))}
          </div>
        ) : null}

        <br />
        <br />
      </div>
      <br />

      <br />

      <div className="container overflow-hidden">
        <div className="row g-3">
          <div className="col-sm">
            <h5>Stats</h5>
            <ul className="list-group">
              <li className="list-group-item">200 total commits</li>
              <li className="list-group-item">50 total issues</li>
              <li className="list-group-item">48 total unit tests</li>
              <li className="list-group-item">
                <a href="https://www.postman.com/findjobsswe373/workspace/team-workspace/overview?ctx=settings">
                  Postman API
                </a>
              </li>
              <li className="list-group-item">
                <a href="https://gitlab.com/brandon.v.lam/cs373-idb/-/issues">
                  GitLab Issue Tracker
                </a>
              </li>
              <li className="list-group-item">
                <a href="https://gitlab.com/brandon.v.lam/cs373-idb/">
                  GitLab Repo
                </a>
              </li>
              <li className="list-group-item">
                <a href="https://gitlab.com/brandon.v.lam/cs373-idb/-/wikis">
                  GitLab Wiki
                </a>
              </li>
              <li className="list-group-item">
                <a href="https://speakerdeck.com/findjobs/findjobs-idb3-presentation">
                  Presentation Slides on SpeakerDeck
                </a>
              </li>
            </ul>
          </div>
          <div className="col-sm">
            <h5>Data</h5>
            <ul className="list-group">
              <li className="list-group-item">
                <h6>Data Sources</h6>
                <a href="https://developer.adzuna.com/docs/search">
                  Adzuna API
                </a>
                <br />
                <a href="https://www.themuse.com/developers/api/v2">
                  TheMuse API v2
                </a>
                <br />
                <a href="https://openweathermap.org/current">
                  OpenWeatherMap Current Weather API
                </a>
                <br />
                <a href="https://openweathermap.org/api/geocoding-api">
                  OpenWeatherMap Geocoding API
                </a>
              </li>

              <li className="list-group-item">
                <a href="https://documenter.getpostman.com/view/21556224/UzBqoQWv">
                  Published Postman Collection
                </a>
              </li>
              <li className="list-group-item">
                1st pillar scraped from Adzuna through manual GET requests
              </li>
              <li className="list-group-item">
                2nd pillar scraped from TheMuse through manual GET requests
              </li>
              <li className="list-group-item">
                3rd pillar scraped from OpenWeatherMap through manual GET
                requests
              </li>
            </ul>
          </div>
          <div className="col-sm">
            <h5>Tools</h5>
            <ul className="list-group">
              <li className="list-group-item">
                <h6>Frontend Tools</h6>
                <p>Bootstrap, React.js, React Query, React Table</p>
              </li>
              <li className="list-group-item">
                <h6>Backend Tools</h6>
                <p>Flask, Postman, GCP, PostgresSql, SqlAlchemy</p>
              </li>
              <li className="list-group-item">
                <h6>Other Tools</h6>
                <p>Gitlab, Discord</p>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <br />
    </>
  );
};

export default AboutPage;
