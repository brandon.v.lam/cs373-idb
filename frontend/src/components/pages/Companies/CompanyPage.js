import axios from "axios";
import { useParams } from "react-router-dom";
import { useQuery } from "react-query";
import { useEffect } from "react";

async function getCompany(companyId) {
  try {
    const data = await axios.get(
      `https://idb-3-354621.uc.r.appspot.com/companies/${companyId}`
    );
    return data.data?.Company;
  } catch (err) {
    console.error(err);
  }
}

const CompanyPage = () => {
  useEffect(() => {
    document.title = "Company";
  }, []);

  const params = useParams();

  const { data: company, isLoading } = useQuery(
    ["company", params.companyId],
    () => getCompany(params.companyId),
    {
      enabled: Boolean(params?.companyId),
    }
  );

  return (
    <div className="mt-4 px-4 pb-4">
      {isLoading ? (
        <div aria-hidden="true">
          <div className="justify-content-center text-center fw-bold fs-2">
            <span className="placeholder col-2"></span>
            <span className="placeholder col-2"></span>
          </div>
          <div className="d-flex justify-content-center">
            <div
              className="img-fluid placeholder mb-1"
              style={{ width: "65%", height: "300px" }}
            />
            <br />
          </div>
          <div className="fs-5 fw-bold pt-4">Industry</div>
          <div className="fs-6 placeholder col-6"></div>
          <div className="fs-5 fw-bold pt-4">Description</div>
          <div className="fs-6 placeholder col-12"></div>
          <div className="fs-5 fw-bold pt-4">List of Jobs</div>

          <div className="list-group">
            <span className="placeholder col-12"></span>
            <span className="placeholder col-12"></span>
            <span className="placeholder col-12"></span>
          </div>

          <div className="list-group">
            <div className="fs-5 fw-bold pt-4">Location</div>
            <span className="placeholder col-6"></span>
            <span className="placeholder col-6"></span>
          </div>
        </div>
      ) : company ? (
        <>
          <div className="text-center fw-bold fs-2">{company.name}</div>
          <div className="d-flex justify-content-center">
            <img
              className="img-fluid"
              src={company.logo}
              alt={`logo of ${company.name}`}
              style={{ maxHeight: "300px" }}
              vspace="10px"
            />
          </div>
          <div className="fs-5 fw-bold pt-4">Industry</div>
          <div className="fs-6">{company.industry}</div>
          <div className="fs-5 fw-bold pt-4">Description</div>
          <div className="fs-6">{company.description}</div>
          <div className="fs-5 fw-bold pt-4">List of Jobs</div>
          {company.jobs === [] ? (
            <p>No jobs available</p>
          ) : (
            <div className="list-group">
              {company.jobs.map((job) => (
                <a
                  href={`/jobs/${job.id}`}
                  key={job.id}
                  className="list-group-item list-group-item-action"
                >
                  {job.title}
                </a>
              ))}
            </div>
          )}
          <div className="list-group">
            <div className="fs-5 fw-bold pt-4">Location</div>
            <a
              href={`/locations/${company.location.id}`}
              className="list-group-item list-group-item-action"
            >
              {company.location.city}, {company.location.state}
            </a>
          </div>
        </>
      ) : null}
    </div>
  );
};

export default CompanyPage;
