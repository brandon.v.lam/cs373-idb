/* eslint-disable jsx-a11y/anchor-has-content */
import axios from "axios";
import { useMemo, useEffect, useState } from "react";
import { useQuery } from "react-query";
import Highlighter from "../../shared/Highlighter";
import Table from "../../shared/Table";

async function getCompanies(query = null) {
  try {
    const url = !query
      ? "https://idb-3-354621.uc.r.appspot.com/companies/"
      : "https://idb-3-354621.uc.r.appspot.com/companies?search=" + query;
    const data = await axios.get(url);
    return data.data;
  } catch (err) {
    console.error(err);
  }
}

const CompaniesPage = () => {
  useEffect(() => {
    document.title = "Companies";
  }, []);

  const [query, setQuery] = useState(null);

  const { data, isLoading } = useQuery(["companies", query], () =>
    getCompanies(query)
  );

  const columns = useMemo(
    () => [
      {
        Header: "Name",
        accessor: "name",
      },
      {
        Header: "Description",
        accessor: "description",
      },
      {
        Header: "City",
        accessor: "location.city",
      },
      {
        Header: "State",
        accessor: "location.state",
      },
      {
        Header: "Industry",
        accessor: "industry",
      },
      {
        Header: "Jobs",
        accessor: "jobs",
        Cell: ({ value, row }) => {
          return value.map((job) => (
            <div key={job.id}>
              <b>-</b>{" "}
              <Highlighter wordsToHighlight={query}>{job.title}</Highlighter>
            </div>
          ));
        },
      },
    ],
    [query]
  );
  return (
    <div className="mt-4 px-4">
      <div className="text-center fw-bold fs-2">Companies</div>
      <Table
        data={data?.Companies || []}
        isLoading={isLoading}
        columns={columns}
        urlToNavigateTo="/companies"
        setQuery={setQuery}
        query={query}
      />
    </div>
  );
};

export default CompaniesPage;
