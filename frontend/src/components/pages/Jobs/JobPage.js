import axios from "axios";
import { useParams } from "react-router-dom";
import { useQuery } from "react-query";
import { useEffect } from "react";

async function getJob(jobId) {
  try {
    const data = await axios.get(
      `https://idb-3-354621.uc.r.appspot.com/jobs/${jobId}`
    );
    return data.data?.Job;
  } catch (err) {
    console.error(err);
  }
}

const JobPage = () => {
  useEffect(() => {
    document.title = "Job";
  }, []);

  const params = useParams();

  const { data: job, isLoading } = useQuery(
    ["job", params.jobId],
    () => getJob(params.jobId),
    {
      enabled: Boolean(params?.jobId),
    }
  );

  return (
    <div className="mt-4 px-4">
      {isLoading ? (
        <div aria-hidden="true">
          <div className="text-center fw-bold fs-2 placeholder col-12"></div>
          <div className="d-flex justify-content-center">
            <p
              className="btn btn-primary disabled mt-2 placeholder col-1"
              href="#"
            ></p>
          </div>

          <div className="fs-5 fw-bold pt-4">Company</div>

          <div className="list-group">
            <span className="placeholder col-12"></span>
            <span className="placeholder col-12"></span>
          </div>

          <div className="fs-5 fw-bold pt-4">Location</div>
          <div className="list-group">
            <span className="placeholder col-12"></span>
            <span className="placeholder col-12"></span>
          </div>
          <div className="fs-5 fw-bold pt-4">Description</div>
          <div className="fs-6">
            <span className="placeholder col-12"></span>
            <span className="placeholder col-12"></span>
            <span className="placeholder col-12"></span>
            <span className="placeholder col-12"></span>
          </div>
          <div className="fs-5 fw-bold pt-4">Date Created</div>
          <div className="fs-6 placeholder col-2"></div>
          <div className="fs-5 fw-bold pt-4">Category</div>
          <div className="fs-6 placeholder col-4"></div>
        </div>
      ) : job ? (
        <>
          <div className="text-center fw-bold fs-2">{job.title}</div>
          <div className="d-flex justify-content-center">
            <a className="btn btn-primary mt-2" href={job.jobUrl} role="button">
              Apply to Job
            </a>
          </div>
          <div className="fs-5 fw-bold pt-4">Company</div>
          {job.company.id ? (
            <div className="list-group">
              <a
                href={`/companies/${job.company.id}`}
                className="list-group-item list-group-item-action"
              >
                {job.company.name}
              </a>
            </div>
          ) : (
            <div className="fs-6">{job.company.name}</div>
          )}
          <div className="fs-5 fw-bold pt-4">Location</div>
          <div className="list-group">
            <a
              href={`/locations/${job.location.id}`}
              className="list-group-item list-group-item-action"
            >
              {job.location.city}, {job.location.state}
            </a>
          </div>
          <div className="fs-5 fw-bold pt-4">Description</div>
          <div className="fs-6">{job.description}</div>
          <div className="fs-5 fw-bold pt-4">Date Created</div>
          <div className="fs-6">{job.createdAt}</div>
          <div className="fs-5 fw-bold pt-4">Category</div>
          <div className="fs-6">{job.category}</div>
        </>
      ) : null}
    </div>
  );
};

export default JobPage;
