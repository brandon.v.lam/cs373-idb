/* eslint-disable jsx-a11y/anchor-has-content */
import { useQuery } from "react-query";
import "./JobsPage.css";
import axios from "axios";
import Table from "../../shared/Table";
import { useMemo, useEffect, useState } from "react";

async function getJobs(query = null) {
  try {
    const url = !query
      ? "https://idb-3-354621.uc.r.appspot.com/jobs/"
      : "https://idb-3-354621.uc.r.appspot.com/jobs?search=" + query;
    const data = await axios.get(url);
    return data.data;
  } catch (err) {
    console.error(err);
  }
}

const Jobs = () => {
  useEffect(() => {
    document.title = "Jobs";
  }, []);

  const [query, setQuery] = useState(null);

  // get data from our api
  const { data, isLoading } = useQuery(["jobs", query], () => getJobs(query));

  const columns = useMemo(
    () => [
      {
        Header: "Title",
        accessor: "title",
      },
      {
        Header: "Description",
        accessor: "description",
      },
      {
        Header: "City",
        accessor: "location.city",
      },
      {
        Header: "State",
        accessor: "location.state",
      },
      {
        Header: "Company",
        accessor: "company.name",
      },
      {
        Header: "Category",
        accessor: "category",
      },
    ],
    []
  );

  return (
    <div className="mt-4 px-4">
      <div className="text-center fw-bold fs-2">Jobs</div>
      <Table
        data={data?.Jobs || []}
        isLoading={isLoading}
        columns={columns}
        urlToNavigateTo="/jobs"
        query={query}
        setQuery={setQuery}
      />
    </div>
  );
};

export default Jobs;
