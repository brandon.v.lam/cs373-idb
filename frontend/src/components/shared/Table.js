import { useTable, useSortBy, usePagination } from "react-table";
import { useNavigate } from "react-router-dom";
import "./Table.css";
import Search from "./Search";
import Highlighter from "./Highlighter";
const Table = ({
  columns,
  data,
  urlToNavigateTo,
  isLoading,
  defaultPageSize = 10,
  setQuery = null,
  query = null,
  showSearch = true,
}) => {
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    state: { pageIndex },
  } = useTable(
    {
      columns,
      data: data,
      initialState: { pageSize: defaultPageSize },
    },
    useSortBy,
    usePagination
  );

  const navigate = useNavigate();

  return (
    <>
      <div className="col fw-semibold d-flex justify-content-center">
        Click a header to sort!
      </div>

      <div className="d-flex align-items-center row pb-2">
        <div className="d-flex col align-items-center">
          <div
            className="btn-group d-flex align-items-center"
            style={{ maxWidth: "300px" }}
            role="group"
          >
            <button
              onClick={() => gotoPage(0)}
              className="btn btn-primary"
              disabled={!canPreviousPage || isLoading}
            >
              {"<<"}
            </button>{" "}
            <button
              className="btn btn-primary"
              onClick={() => previousPage()}
              disabled={!canPreviousPage || isLoading}
            >
              {"<"}
            </button>{" "}
            <button
              className="btn btn-primary"
              onClick={() => nextPage()}
              disabled={!canNextPage || isLoading}
            >
              {">"}
            </button>{" "}
            <button
              className="btn btn-primary"
              onClick={() => gotoPage(pageCount - 1)}
              disabled={!canNextPage || isLoading}
            >
              {">>"}
            </button>{" "}
          </div>
          <span>
            Page{" "}
            <strong>
              {pageIndex + 1} of {pageOptions.length}
            </strong>{" "}
          </span>
        </div>

        {showSearch && (
          <div className="d-flex w-100 justify-content-end col">
            <Search setQuery={setQuery} isLoading={isLoading} />
          </div>
        )}
      </div>
      <div style={{ width: "100%", overflow: "auto" }}>
        <table {...getTableProps()} style={{ width: "100%" }}>
          <thead>
            {headerGroups.map((headerGroup) => (
              <tr {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map((column) => (
                  <th {...column.getHeaderProps(column.getSortByToggleProps())}>
                    {" "}
                    {column.render("Header")}
                    <span>
                      {column.isSorted
                        ? column.isSortedDesc
                          ? " 🔽"
                          : " 🔼"
                        : ""}
                    </span>
                  </th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody {...getTableBodyProps()} style={{ width: "100%" }}>
            {data
              ? page.map((row, i) => {
                  prepareRow(row);
                  return (
                    <tr
                      {...row.getRowProps()}
                      style={{ cursor: "pointer" }}
                      className="clickable-row"
                      onClick={() =>
                        navigate(`${urlToNavigateTo}/${row.original.id}`)
                      }
                    >
                      {row.cells.map((cell) => {
                        if (typeof cell?.value !== "string") {
                          return (
                            <td
                              style={{ verticalAlign: "top" }}
                              {...cell.getCellProps()}
                            >
                              {cell.render("Cell")}
                            </td>
                          );
                        }
                        return (
                          <td
                            style={{ verticalAlign: "top" }}
                            {...cell.getCellProps()}
                          >
                            <Highlighter wordsToHighlight={query}>
                              {cell.render("Cell")}
                            </Highlighter>
                          </td>
                        );
                      })}
                    </tr>
                  );
                })
              : null}
          </tbody>
        </table>
        {isLoading ? (
          <div className="d-flex justify-content-center fw-semibold pt-4">
            <h3>Loading Data...</h3>
          </div>
        ) : data?.length === 0 ? (
          <div className="d-flex justify-content-center fw-semibold pt-4">
            <h3>No results</h3>
          </div>
        ) : null}
      </div>
      <br />
    </>
  );
};

export default Table;
