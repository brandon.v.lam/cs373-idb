import React from "react";

const Highlighter = ({ wordsToHighlight, children }) => {
  const wordsToMatchArr = wordsToHighlight?.toLowerCase()?.trim()?.split(" ");

  const textArr =
    typeof children === "string"
      ? children.split(" ")
      : Boolean(children?.props?.cell?.value)
      ? children?.props?.cell?.value?.split(" ")
      : [];

  return (
    <>
      {textArr?.map((word, index) => (
        <React.Fragment key={index}>
          {wordsToHighlight &&
          wordsToMatchArr.some((matchable) =>
            word.toLowerCase().includes(matchable)
          ) ? (
            <>
              <span className="bg-warning">{word}</span>{" "}
            </>
          ) : (
            <>{word} </>
          )}
        </React.Fragment>
      ))}
    </>
  );
};

export default Highlighter;
