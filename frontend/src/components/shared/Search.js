import React, { useState } from "react";

const Search = ({ setQuery, isLoading = false }) => {
  const [value, setValue] = useState("");

  return (
    <>
      <input
        className="form-control me-2"
        type="search"
        placeholder="Search"
        aria-label="Search"
        style={{ maxWidth: "400px", minWidth: "160px" }}
        value={value}
        onChange={(e) => setValue(e.target.value)}
        onKeyDown={(e) => {
          if (e.key === "Enter") {
            setQuery(value);
          }
        }}
      />
      <button
        className="btn btn-outline-success"
        type="button"
        disabled={isLoading}
        onClick={() => setQuery(value)}
      >
        {isLoading ? (
          <>
            <span
              className="spinner-border spinner-border-sm pr-2"
              role="status"
              aria-hidden="true"
            ></span>
            Loading...
          </>
        ) : (
          <>Search</>
        )}
      </button>
    </>
  );
};

export default Search;
