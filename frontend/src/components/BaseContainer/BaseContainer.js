import NavBar from "../NavBar/NavBar";

const BaseContainer = ({ children }) => {
  return (
    <div className="container-fluid p-0">
      <NavBar />
      {children}
    </div>
  );
};

export default BaseContainer;
