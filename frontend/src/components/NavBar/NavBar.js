import { useLocation } from "react-router-dom";

const NavLink = ({ href, label }) => {
  const location = useLocation();

  const isActive = location.pathname === href; // if url matches link - true

  const classNameName = `nav-link ${isActive && "active"}`;

  return (
    <li className="nav-item">
      <a className={classNameName} href={href}>
        {label}
      </a>
    </li>
  );
};

const NavBar = () => {
  return (
    <nav className="navbar navbar-expand-lg bg-dark navbar-dark">
      <div className="container-fluid fs-5 d-flex align-items-center h-100">
        <a className="navbar-brand text-warning" href="/">
          FindJobs
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <NavLink label="Jobs" href="/jobs" />
            <NavLink label="Companies" href="/companies" />
            <NavLink label="Locations" href="/locations" />
            <NavLink label="About" href="/about" />
            <NavLink label="Models" href="/models" />
            <NavLink label="Search" href="/search" />
            <NavLink label="Visualization" href="/visualization" />
          </ul>
        </div>
      </div>
    </nav>
  );
};
export default NavBar;
