import "./App.css";
import BaseContainer from "./components/BaseContainer/BaseContainer";
import { Route, Routes } from "react-router-dom";
import JobsPage from "./components/pages/Jobs/JobsPage";
import CompaniesPage from "./components/pages/Companies/CompaniesPage";
import CompanyPage from "./components/pages/Companies/CompanyPage";
import LocationsPage from "./components/pages/Locations/LocationsPage";
import JobPage from "./components/pages/Jobs/JobPage";
import LocationPage from "./components/pages/Locations/LocationPage";
import SplashPage from "./components/pages/Splash/SplashPage";
import AboutPage from "./components/pages/About/AboutPage";
import ModelsPage from "./components/pages/Models/ModelsPage";
import SearchPage from "./components/pages/Search/SearchPage";
import VisualizationPage from "./components/pages/Visualization/VisualizationPage";

function App() {
  return (
    <BaseContainer>
      <Routes>
        <Route path="/" element={<SplashPage />} />
        <Route path="/jobs" element={<JobsPage />} />
        <Route path="/companies" element={<CompaniesPage />} />
        <Route path="/locations" element={<LocationsPage />} />
        <Route path="/about" element={<AboutPage />} />
        <Route path="/models" element={<ModelsPage />} />
        <Route path="/search" element={<SearchPage />} />
        <Route path="/visualization" element={<VisualizationPage />} />
        <Route path="/jobs/:jobId" element={<JobPage />} />
        <Route path="/companies/:companyId" element={<CompanyPage />} />
        <Route path="/locations/:locationId" element={<LocationPage />} />
      </Routes>
    </BaseContainer>
  );
}

export default App;
